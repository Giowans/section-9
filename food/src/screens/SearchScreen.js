import React, {useState} from 'react';
import {View, StyleSheet, Text} from 'react-native';
import SearchBar from '../components/SearchBar';
import yelp from '../api/yelp';

const SearchScreen = () =>
{
    const [results, setResutls] =  useState([]);
    const [text, setText] = useState('');
    const [errMessage, setErrMessage] = useState('');
    const searchApi = async () =>
    {
        try
        {
            const response =  await yelp.get('/search', {
                params: {
                    limit: 50,
                    term: text,
                    location: 'san jose'
                }
            });
            setResutls(response.data.businesses);
        }catch(err){
            setErrMessage('Algo salió mal :(');
        }
    };
    return (
        <View>
            <SearchBar 
                term = {text} 
                onTermChange = {setText}
                onTermSubmited = {searchApi}
            />
            {errMessage ? <Text>{errMessage}</Text>: null}
            <Text>Hemos obtenido {results.length} resultados</Text>
        </View>
    );
};

const styles = StyleSheet.create({});

export default SearchScreen;