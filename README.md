# Sección 9: Consultas a nuestra API

En esta sección usamos nuestra API creada en yelp.com (ver documentación de la sección anterior).
Para poder hacer consultas a la API usamos un objeto de una libreria que instalamos, llamada `axio`.

**axio** tiene todos los métodos y propiedades necesarios para conectarnos y hacer POST y GET a nuestra REST API de manera sencilla. Tal como lo podemos ver en `../src/api/yelp.js` creamos un componente que nos conecte a la URL dada (_baseURL_) dando las credenciales necesarias (_Authorization_). Este componente puede ser reutilizidado por cualquier otro dentro de nuestra app que necesite sus datos.

El resto de la sección fue implementar nuestro componente de **axio** en nuestro `SearchScreen` usando estados y hasta vimos una introducción para percibir los casos en donde nuestra API pueda fallar y así manejar los errores para darle al usuario una interfaz bastante intuitiva.

### Notas:
- Existe una forma de hacer manejo de las API. Este método se llama `fetch`, sin embargo Stephen menciona que es un poco complejo de usar...
- En San Juan, California, hay muchos negocios que venden tacos :D

